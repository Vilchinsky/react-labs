import React, {Component} from 'react';
import './App.css';
import Source from './Source/Source';
import CharCounter from './CharCounter/CharCounter';
import ValidationComponent from './ValidationComponent/ValidationComponent'
import CharComponent from './CharComponent/CharComponent'


class App extends Component {
    state = {
        charCounter: 0,
        chars: [],
        inputValue: ''
    };

    inputValueChangeHandler = (e) => {
        let val = e.target.value;
        this.setState({
            charCounter: val.length,
            chars: val.split(''),
            inputValue: val
        })
    };

    charClickHandler = (index) => {
        let chars = [...this.state.chars];
        chars.splice(index,1);
        this.setState({
            chars: chars,
            charCounter: chars.length,
            inputValue: chars.join('')
        })
    };

    render() {

        const {charCounter, inputValue, chars} = this.state;

        const charsList = (
            <div className="chars">
                {chars.map((char, index) => {
                    return <CharComponent key={index} value={char} click={() => this.charClickHandler(index)}/>
                })}
            </div>
        );

        return (
            <div className="App">
                <Source change={this.inputValueChangeHandler} value={inputValue}/>
                <CharCounter value={charCounter}/>
                <ValidationComponent value={charCounter}/>
                {charsList}
            </div>
        );
    }
}

export default App;
