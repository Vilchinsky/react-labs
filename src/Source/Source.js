import React, {Component} from 'react';
import './Source.css'

class Source extends Component {

    render() {

        const {change, value} = this.props;

        return <input className="source" type="text" onChange={change} value={value} />
    }

}

export default Source